var Application = function() {};

Application.prototype.start = function() {
  var log = require('./lib/log.js');
  var config = require('config').Api;
  var restify = require('restify');
  var server = restify.createServer({
    name: "REST API"
  });

  
  require('./api/routes/routes.js')(server);

  server.listen(config.port, function() {
    log.info('Server listening on port %s', config.port);
  });
};

var application = new Application();
application.start();