#!/usr/bin/python

import time
import sys
import argparse
import string
import subprocess
import itertools

parser = argparse.ArgumentParser(description='watch command')
parser.add_argument('-n', '--interval', type=int, default=2,
                    help='time in which to wait between iterations (default: 2)')
parser.add_argument('cmd', nargs=argparse.REMAINDER)
args = parser.parse_args()

print string.Template("Watching every $time seconds: $cmd") \
            .substitute(time=args.interval, cmd=' '.join(args.cmd))

for i in itertools.count():
  if i % 10 == 0:
    print 'iteration: ' + `i`
  process = subprocess.Popen(args.cmd).wait()
  time.sleep(args.interval)
