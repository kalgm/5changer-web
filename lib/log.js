var winston = require('winston');
var config = require('config').Logger;

var log = new (winston.Logger)({
  transports: [
    new(winston.transports.Console)({ 
      level: config.level,
      json: false,
      timestamp: true,
      prettyPrint: true
    }),
    new(winston.transports.File)({
      level: config.level,
      json: false,
      timestamp: true,
      filename: config.dir + '/' + config.file,
      prettyPrint: true
    })
  ],
  exitOnError: false
});

module.exports = log;