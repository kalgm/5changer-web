var Application = function() {};

Application.prototype.start = function() {
  var log = require('./lib/log.js');
  var config = require('config').App;
  var express = require('express');
  var server = express();

  require('./app/controllers/routes.js')(server);

  server.engine('html', require('ejs').renderFile);
  server.set('view_engine', 'html');
  server.listen(config.port, function() {
    log.info('Server listening on port %s', config.port);
  });
};

var application = new Application();
application.start();