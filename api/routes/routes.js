var log = require('lib/log.js');

var routes = function(server) {
  server.get('/', function(req, res, next) {
    res.send({"message": "Hello World!"});
  });

  server.use(function(err, req, res, next) {
    if(err.stack) {
      log.error(err.stack);
    } else {
      log.error(JSON.stringify(err));
    }
    res.status(500).send("Catastrophic failure.");
  });
};

module.exports = routes;