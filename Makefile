SHELL = bash

clean:
	rm -rf node_modules
	rm -rf tmp

install: 
	mkdir -p tmp
	npm install
	ln -s ../lib node_modules/lib