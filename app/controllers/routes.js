var log = require('lib/log.js');
var path = require('path');
var Message = require('../models/message.js');

var routes = function(app) {
  app.get('/', function(req, res) {
    res.render(path.join(__dirname, '../views/index.html'), new Message());
  });

  app.use(function(err, req, res, next) {
    console.error(err.stack);
    res.status(500);
  });
};

module.exports = routes;